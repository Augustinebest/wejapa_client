import React, { useState, useEffect, useContext } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import MainNavigation from './src/navigation';
import { ThemeContextProvider } from './src/theme';
import { ThemeContext } from './src/theme';
import AsyncStorage from '@react-native-community/async-storage';
import Splash from './src/components/splash';

const App = () => {
  const Theme = useContext(ThemeContext)
  React.useEffect(() => {
  AsyncStorage.setItem('theme', JSON.stringify(Theme.theme))
  }, [Theme.theme])
  const [splash, setSplash] = useState(true);
    setTimeout(() => {
      setSplash(false)
    }, 2000)
  return (
    <NavigationContainer>
      <ThemeContextProvider>
            <MainNavigation />
      </ThemeContextProvider>
    </NavigationContainer>
  )
}

export default App;