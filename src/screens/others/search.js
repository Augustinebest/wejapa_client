import React, { useState } from 'react';
import { View, TouchableHighlight, TextInput, Text, ScrollView, ImageBackground } from 'react-native';
import { BackwardIcon, GiftIcon } from '../../assets/icons';
import { theme } from '../../theme';
import { search } from '../../store/actions/userActions';
import { openModal } from '../../store/actions/privelegeActions';
import { connect } from 'react-redux';

let Search = (props) => {
    const { white, inputBackground, black, green, dash_bg, dash_font, dash_border2 } = theme();
    const [jobs, setJobs] = useState(false)
    const [search, setSearch] = useState(false)
    const openModal = (job) => {
        props.openModal(job)
    }

    const handleSearch = (text) => {
        setSearch(text)
        props.search(text).then(res => {
            if (res.status) {
                setJobs(res.data)
            } else {
                setJobs(res.data)
            }
        })
        .catch(err => {
            if(err) setJobs([])
        })
    }

    return (
        <View style={{ flex: 1, backgroundColor: dash_bg }}>
            <View style={{ paddingBottom: 30, justifyContent: 'flex-start', flexDirection: 'row' }}><TouchableHighlight underlayColor={null} onPress={() => props.navigation.goBack()} style={{ padding: 15 }}><BackwardIcon /></TouchableHighlight></View>
            <ScrollView style={{ flex: 1, paddingHorizontal: 20 }}>
                <View style={{ paddingTop: 10 }}><Text style={{ fontSize: 30, opacity: 0.6, fontWeight: "bold" }}>Search For Jobs</Text></View>
                <TextInput
                    placeholder='Search jobs'
                    style={{ backgroundColor: white, marginBottom: 50, marginTop: 30, fontSize: 20, borderRadius: 10, paddingLeft: 26, paddingVertical: 20, borderWidth: 1, borderColor: dash_border2 }}
                    onChangeText={(text) => handleSearch(text)}
                    value={search}

                />
                {
                    jobs.length > 0 ?
                        jobs.map((job, index) => {
                            return (
                                <TouchableHighlight onPress={() => openModal(job)} key={index} underlayColor={white} style={{ backgroundColor: white, padding: 15, borderRadius: 10, marginBottom: 20 }}>
                                <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
                                    <View style={{ width: 64, height: 64, borderWidth: 1, justifyContent: 'center', alignItems: 'center', borderRadius: 32, backgroundColor: black, zIndex: 3 }}>
                                        <ImageBackground source={{ uri: `https://res.cloudinary.com/chota/image/upload/v1590707322/myweja.png` }} style={{ width: 35, height: 35 }}></ImageBackground>
                                    </View>
                                    <View style={{ flex: 1, marginHorizontal: 20, justifyContent: 'center' }}>
                                        <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{job.name}</Text>
                                        <View style={{ flexDirection: 'row', marginTop: 5 }}><Text>{job.location} - </Text><Text style={{ color: green }}>{job.status}</Text></View>
                                    </View>
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{marginBottom: 5}}><GiftIcon /></Text>
                                        <Text>${job.salary}</Text>
                                    </View>
                                </View>
                            </TouchableHighlight>
                            )
                        })
                        // <Text>Nna eh</Text>
                        : <Text style={{color: dash_font}}>{!jobs ? '' : 'No search result found'}</Text>
                }
            </ScrollView>
        </View>
    )
}

Search = connect(null, { search, openModal })(Search);

export { Search }