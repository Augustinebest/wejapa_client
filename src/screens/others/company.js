import React from 'react';
import { View, Text, TouchableHighlight, ScrollView, ImageBackground } from 'react-native';
import { BackwardIcon, GiftIcon } from '../../assets/icons';
import { theme } from '../../theme';

const Company = (props) => {
    const { inputBackground, white, black, green } = theme();
    const items = [1, 2, 3, 4, 5, 6, 7, 8]
    return (
        <View style={{ flex: 1 }}>
            <View style={{ paddingBottom: 30, justifyContent: 'flex-start', flexDirection: 'row' }}><TouchableHighlight underlayColor={null} onPress={() => props.navigation.goBack()} style={{ padding: 15 }}><BackwardIcon /></TouchableHighlight></View>
            <ScrollView style={{ backgroundColor: inputBackground, flex: 1, paddingHorizontal: 20 }}>
                <View style={{paddingTop: 10}}><Text style={{ fontSize: 30, opacity: 0.6, fontWeight: "bold" }}>Jobs From Wejapa</Text></View>
                <View style={{ marginTop: 30, marginBottom: 40 }}>
                    {
                        items.map((item, index) => {
                            return (
                                <TouchableHighlight key={index} style={{ backgroundColor: white, padding: 15, borderRadius: 10, marginBottom: 20 }}>
                                    <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
                                        <View style={{ width: 64, height: 64, borderWidth: 1, justifyContent: 'center', alignItems: 'center', borderRadius: 32, backgroundColor: black, zIndex: 3 }}>
                                            <ImageBackground source={require('../../assets/myweja.png')} style={{ width: 35, height: 35 }}></ImageBackground>
                                        </View>
                                        <View style={{ flex: 1, marginHorizontal: 20, justifyContent: 'center' }}>
                                            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Flutter developer</Text>
                                            <View style={{ flexDirection: 'row', marginTop: 5 }}><Text>Remote (USA) - </Text><Text style={{ color: green }}>open</Text></View>
                                        </View>
                                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{marginBottom: 5}}><GiftIcon /></Text>
                                            <Text>$650</Text>
                                        </View>
                                    </View>
                                </TouchableHighlight>
                            )
                        })
                    }
                </View>
            </ScrollView>
        </View>
    )
}

export { Company };