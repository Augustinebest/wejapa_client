import React, { useEffect, useState, useContext } from 'react';
import { View, Text, ScrollView, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import { theme, ThemeContext } from '../../theme';
import { getProfile, updateProfile } from '../../store/actions/userActions';
import { setAuthState } from '../../store/actions/authActions';
import { connect } from 'react-redux';
import validator from '../../utils/validator';
import { BrightIcon, DarkIcon } from '../../assets/icons';

let Profile = (props) => {

    const { green, white, black, errorOutline, dash_bg, dash_font, dash_border, dash_inputBackground, dash_signOut_btn, dash_border2 } = theme();
    const [control, setControl] = useState({ isDark: false });
    const [err, setErr] = useState({ phoneErr: null, portfolioErr: null, stackoverflowErr: null, resumeErr: null })
    useEffect(() => {
        props.getProfile()
    }, [])

    let Theme = useContext(ThemeContext)

    const profile = props.auth

    const handleChange = (name, value) => {
        props.setAuthState({ [name]: value })
        setErr({ ...err, phoneErr: null, portfolioErr: null, stackoverflowErr: null, resumeErr: null })
    }

    const updateProfile = () => {
        const { phone, portfolio, stackoverflow, resume } = profile;
        if (!validator('phone', phone)) {
            setErr({ ...err, phoneErr: 'Enter Number with country code' })
        } else if (!validator('url', portfolio)) {
            setErr({ ...err, portfolioErr: 'Github/Portfolio/Website url required' })
        } else if (!validator('url', stackoverflow)) {
            setErr({ ...err, stackoverflowErr: 'Invalid Stackoverflow link' })
        } else if (!validator('url', resume)) {
            setErr({ ...err, resumeErr: 'Resume link is required' })
        } else {
            props.updateProfile(profile)
        }
    }

    const toggleTheme = () => {
        setControl({ isDark: !control.isDark })
        console.log('control: ', control.isDark);
        Theme.setTheme(control.isDark ? 'dark' : 'light')
    }

    console.log('control outside: ', control.isDark)

    return (
        <ScrollView style={{ paddingHorizontal: 20, paddingVertical: 40, backgroundColor: dash_bg }}>
            <View style={{ marginBottom: 100 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}><Text style={{ fontSize: 30, opacity: 0.8, fontWeight: "bold", color: dash_font }}>Your Profile</Text><TouchableOpacity onPress={toggleTheme}>{!control.isDark ? <DarkIcon /> : <BrightIcon />}</TouchableOpacity></View>
                <View style={{ marginTop: 40, paddingVertical: 22, paddingHorizontal: 30, borderRadius: 10, flexDirection: 'row', justifyContent: 'flex-start', backgroundColor: dash_inputBackground, borderWidth: 1, borderColor: dash_border2 }}><Text style={{ opacity: 0.8, fontWeight: "bold", color: dash_font }}>{profile && profile.fullName}</Text></View>
                <View style={{ marginTop: 40, paddingVertical: 22, paddingHorizontal: 30, borderRadius: 10, flexDirection: 'row', justifyContent: 'flex-start', backgroundColor: dash_inputBackground, borderWidth: 1, borderColor: dash_border2 }}><Text style={{ opacity: 0.8, fontWeight: "bold", color: dash_font }}>{profile && profile.email}</Text></View>
                <TextInput
                    placeholder='Phone number'
                    value={profile && profile.phone}
                    onChangeText={(text) => handleChange('phone', text)}
                    style={{ backgroundColor: dash_inputBackground, color: dash_font, marginTop: 30, fontWeight: 'bold', borderRadius: 10, paddingLeft: 26, paddingVertical: 18, borderWidth: 1, borderColor: err.phoneErr ? errorOutline : dash_border2 }}
                />
                <Text style={{ paddingHorizontal: 30, paddingTop: 4, color: dash_font, fontWeight: 'bold', opacity: 0.8 }}>{err.phoneErr ? err.phoneErr : null}</Text>
                <TextInput
                    placeholder='Github/Portfolio/Website link'
                    value={profile && profile.portfolio}
                    onChangeText={(text) => handleChange('portfolio', text)}
                    style={{ backgroundColor: dash_inputBackground, color: dash_font, marginTop: 15, fontWeight: 'bold', borderRadius: 10, paddingLeft: 26, paddingVertical: 18, borderWidth: 1, borderColor: err.portfolioErr ? errorOutline : dash_border2 }}
                />
                <Text style={{ paddingHorizontal: 30, paddingTop: 4, color: dash_font, fontWeight: 'bold', opacity: 0.8 }}>{err.portfolioErr ? err.portfolioErr : null}</Text>
                <TextInput
                    placeholder='Stackoverflow link'
                    placeholderTextColor={dash_font}
                    value={profile && profile.stackoverflow}
                    onChangeText={(text) => handleChange('stackoverflow', text)}
                    style={{ backgroundColor: dash_inputBackground, color: dash_font, marginTop: 15, fontWeight: 'bold', borderRadius: 10, paddingLeft: 26, paddingVertical: 18, borderWidth: 1, borderColor: err.stackoverflowErr ? errorOutline : dash_border2 }}
                />
                <Text style={{ paddingHorizontal: 30, paddingTop: 4, color: dash_font, fontWeight: 'bold', opacity: 0.8 }}>{err.stackoverflowErr ? err.stackoverflowErr : null}</Text>
                <TextInput
                    placeholder='Resume'
                    value={profile && profile.resume}
                    onChangeText={(text) => handleChange('resume', text)}
                    style={{ backgroundColor: dash_inputBackground, color: dash_font, marginTop: 15, fontWeight: 'bold', borderRadius: 10, paddingLeft: 26, paddingVertical: 18, borderWidth: 1, borderColor: err.resumeErr ? errorOutline : dash_border2 }}
                />
                <Text style={{ paddingHorizontal: 30, paddingTop: 4, color: dash_font, fontWeight: 'bold', opacity: 0.8 }}>{err.resumeErr ? err.resumeErr : null}</Text>
                <TouchableHighlight underlayColor={green} onPress={updateProfile} style={{ backgroundColor: green, marginTop: 50, padding: 20, borderRadius: 10, flexDirection: 'row', justifyContent: 'center', opacity: 0.7 }}>
                    <Text style={{ fontWeight: '300', color: white }}>Update Profile</Text>
                </TouchableHighlight>
                <TouchableHighlight underlayColor={black} onPress={() => props.navigation.navigate('Login')} style={{ backgroundColor: dash_signOut_btn, marginTop: 15, padding: 20, borderRadius: 10, flexDirection: 'row', justifyContent: 'center', opacity: 0.7 }}>
                    <Text style={{ fontWeight: '300', color: white }}>Sign out</Text>
                </TouchableHighlight>
            </View>
        </ScrollView>
    )
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
}

Profile = connect(mapStateToProps, { getProfile, updateProfile, setAuthState })(Profile)

export { Profile };