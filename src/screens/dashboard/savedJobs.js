import React from 'react';
import { View, Text } from 'react-native';
import { theme } from '../../theme';

const SavedJobs = () => {
    const { dash_bg, dash_font, dash_inputBackground_2, black } = theme();
    return (
        <View style={{flex: 1, paddingHorizontal: 20, backgroundColor: dash_bg}}>
            <View style={{paddingTop: 60}}><Text style={{ fontSize: 30, opacity: 0.9, fontWeight: "bold", color: dash_font }}>Saved Jobs</Text></View>
            <View style={{marginTop: 30, padding: 20, borderRadius: 10, flexDirection: 'row', justifyContent: 'center', backgroundColor: dash_inputBackground_2}}><Text style={{color: black}}>Nothing to show</Text></View>
        </View>
    )
}

export { SavedJobs };