import React, { useEffect } from 'react';
import { View, Text, ScrollView, TouchableOpacity, TouchableHighlight, ImageBackground } from 'react-native';
import { theme } from '../../theme';
import { dashboardGetAllJobs } from '../../store/actions/userActions';
import { PinIconI } from '../../assets/icons';
import { GiftIcon } from '../../assets/icons';

// actions
import { openModal } from '../../store/actions/privelegeActions';
import { connect } from 'react-redux';

var Jobs = (props) => {
    const { white, black, green, dash_font, dash_bg, dash_border2 } = theme();
    const company = [1, 2];

    useEffect(() => {
        props.dashboardGetAllJobs();
    }, [])

    const openModal = (job) => {
        props.openModal(job)
    }

    const jobs = props.job.jobs;
    return (
        <ScrollView style={{ flex: 1, paddingVertical: 60, backgroundColor: dash_bg }}>
            <View style={{ paddingHorizontal: 25 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}><Text style={{ fontSize: 30, opacity: 0.9, fontWeight: "bold", color: dash_font }}>New Jobs</Text><TouchableHighlight underlayColor={null} onPress={() => { props.navigation.navigate('Saved Jobs') }}><PinIconI width="35" height="35" /></TouchableHighlight></View>
                <View style={{ marginTop: 30 }}>
                    <TouchableHighlight
                        underlayColor={white}
                        onPress={() => props.navigation.navigate('Search')}
                        style={{ backgroundColor: white, borderRadius: 10, paddingLeft: 26, paddingVertical: 20, borderWidth: 1, borderColor: dash_border2 }}
                    >
                        <Text style={{ fontSize: 20, opacity: 0.5 }}>Search Jobs</Text>
                    </TouchableHighlight>
                </View>
                <View style={{ marginTop: 30, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <Text style={{ fontSize: 20, opacity: 0.9, fontWeight: 'bold', color: dash_font }}>Lastest Roles</Text><TouchableOpacity onPress={() => { props.navigation.navigate('LatestRoles') }}><Text style={{color: dash_font}}>View All>></Text></TouchableOpacity>
                </View>
                <View style={{ marginTop: 30 }}>
                    {
                        jobs && jobs.length > 0
                            ?
                            jobs.map((job, index) => {
                                return (
                                    <TouchableHighlight onPress={() => openModal(job)} underlayColor={white} key={index} style={{ backgroundColor: white, padding: 15, borderRadius: 10, marginBottom: 20, borderWidth: 1, borderColor: dash_border2 }}>
                                        <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
                                            <View style={{ width: 64, height: 64, borderWidth: 1, justifyContent: 'center', alignItems: 'center', borderRadius: 32, backgroundColor: black, zIndex: 3 }}>
                                                <ImageBackground source={{uri: `${job.image}`}} style={{ width: 35, height: 35 }}></ImageBackground>
                                            </View>
                                            <View style={{ flex: 1, marginHorizontal: 20, justifyContent: 'center' }}>
                                                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{job.name}</Text>
                                                <View style={{ flexDirection: 'row', marginTop: 5 }}><Text>{job.location} - </Text><Text style={{ color: green }}>{job.status}</Text></View>
                                            </View>
                                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                <Text style={{marginBottom: 5}}><GiftIcon /></Text>
                                                <Text>${job.salary}</Text>
                                            </View>
                                        </View>
                                    </TouchableHighlight>
                                )
                            })
                            :
                            <Text style={{color: dash_font}}>There is no job currently</Text>
                    }
                    <View style={{ marginTop: 30, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <Text style={{ fontSize: 20, opacity: 0.9, fontWeight: 'bold', color: dash_font }}>Companies</Text><Text style={{color: dash_font}}>View All>></Text>
                    </View>
                </View>
            </View>
            <View style={{ marginBottom: 100, marginTop: 20, flexDirection: 'row', flexWrap: 'wrap', padding: 10 }}>
                {
                    company.map((comp, index) => {
                        return (
                            <TouchableHighlight underlayColor={white} key={index} style={{ marginBottom: 20, marginLeft: 10, marginRight: 10, borderWidth: 1, borderColor: dash_border2, borderRadius: 10 }} onPress={() => props.navigation.navigate('Company')}>
                                <View style={{ backgroundColor: white, alignItems: 'center', paddingVertical: 30, paddingHorizontal: 30, borderRadius: 10 }}>
                                    <View style={{ width: 64, height: 64, borderWidth: 1, justifyContent: 'center', alignItems: 'center', borderRadius: 32, backgroundColor: black, zIndex: 3 }}>
                                        <ImageBackground source={require('../../assets/myweja.png')} style={{ width: 35, height: 35 }}></ImageBackground>
                                    </View>
                                    <Text style={{ marginTop: 15, fontWeight: 'bold', fontSize: 18 }}>wejapa</Text>
                                    <Text>5 Open Roles</Text>
                                </View>
                            </TouchableHighlight>
                        )
                    })
                }
            </View>
        </ScrollView>
    )
}

const mapStateToProps = (state) => {
    return {
        job: state.job
    }
}

Jobs = connect(mapStateToProps, { openModal, dashboardGetAllJobs })(Jobs)

export { Jobs };