import React, { useState, useEffect } from 'react';
import { View, Text, ScrollView, TouchableHighlight, StyleSheet, ImageBackground, Animated, Dimensions } from 'react-native';
import { PinIconW, GiftIcon } from '../../assets/icons';
import { theme } from '../../theme';
import { openModal } from '../../store/actions/privelegeActions';
import { getProfile } from '../../store/actions/userActions';
import { connect } from 'react-redux';

let deviceHeight = Dimensions.get('window').height
let Applications = (props) => {
    const [active, setActive] = useState(1);

    const { white, green, yellow, black, applications_bg, dash_font } = theme();
    const inProgress = [1, 2, 3, 4, 5];

    const openModal = () => {
        // props.openModal()
    }

    useEffect(() => {
        props.getProfile()
    }, [])

    const { jobs } = props.auth;
    const progress = jobs && jobs.filter(job => job.status === 'progress')
    const rejected = jobs && jobs.filter(job => job.status === 'rejected')
    const accepted = jobs && jobs.filter(job => job.status === 'accepted')
    return (
        <>
            {/* // <View><Text>hffhfh</Text></View> */}
            <ScrollView style={{ paddingHorizontal: 20, flex: 1, backgroundColor: applications_bg }}>
                <View style={{ marginBottom: 30 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 70 }}><View><Text style={{ fontSize: 40, opacity: 0.9, fontWeight: "bold", color: white }}>Your</Text><Text style={{ fontSize: 40, opacity: 0.9, fontWeight: "bold", color: white }}>Applications</Text></View><TouchableHighlight underlayColor={null} onPress={() => { props.navigation.navigate('Saved Jobs') }}><PinIconW width="35" height="35" /></TouchableHighlight></View>
                    <View style={{ marginTop: 40, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <TouchableHighlight underlayColor={null} onPress={() => setActive(1)}><Text style={{ color: white, fontSize: 20, paddingBottom: 10, fontWeight: "bold", borderBottomWidth: active === 1 ? 3 : 0, borderBottomColor: yellow }}>In Progress</Text></TouchableHighlight>
                        <TouchableHighlight underlayColor={null} onPress={() => setActive(2)}><Text style={{ color: white, fontSize: 20, paddingBottom: 10, fontWeight: "bold", borderBottomWidth: active === 2 ? 3 : 0, borderBottomColor: yellow }}>Rejected</Text></TouchableHighlight>
                        <TouchableHighlight underlayColor={null} onPress={() => setActive(3)}><Text style={{ color: white, fontSize: 20, paddingBottom: 10, fontWeight: "bold", borderBottomWidth: active === 3 ? 3 : 0, borderBottomColor: yellow }}>Accepted</Text></TouchableHighlight>
                    </View>
                    <View style={{ marginTop: 60 }}>
                        {
                            active === 1 ?
                                <View>
                                    {
                                        progress && progress.length > 0 ?
                                            progress.map((prog, index) => {
                                                return (
                                                    <TouchableHighlight underlayColor={white} onPress={openModal} key={index} style={{ backgroundColor: white, padding: 15, borderRadius: 10, marginBottom: 20 }}>
                                                        <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
                                                            <View style={{ width: 64, height: 64, borderWidth: 1, justifyContent: 'center', alignItems: 'center', borderRadius: 32, backgroundColor: black, zIndex: 3 }}>
                                                                <ImageBackground source={{ uri: `${prog.jobid.image}` }} style={{ width: 35, height: 35 }}></ImageBackground>
                                                            </View>
                                                            <View style={{ flex: 1, marginHorizontal: 20, justifyContent: 'center' }}>
                                                                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{prog.jobid.name}</Text>
                                                                <View style={{ flexDirection: 'row', marginTop: 5 }}><Text>{prog.jobid.location} - </Text><Text style={{ color: green }}>{prog.jobid.status}</Text></View>
                                                            </View>
                                                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                                <Text style={{ marginBottom: 5 }}><GiftIcon /></Text>
                                                                <Text>${prog.jobid.salary}</Text>
                                                            </View>
                                                        </View>
                                                    </TouchableHighlight>
                                                )
                                            })
                                            // <Text>Fine</Text>
                                            : <Text style={{ color: dash_font }}>You have not applied for any job</Text>
                                    }
                                </View>
                                :
                                active === 2 ?
                                    <View>
                                        {
                                            rejected && rejected.length > 0 ? rejected.map((reject, index) => {
                                                return (
                                                    <TouchableHighlight underlayColor={white} onPress={openModal} key={index} style={{ backgroundColor: white, padding: 15, borderRadius: 10, marginBottom: 20 }}>
                                                        <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
                                                            <View style={{ width: 64, height: 64, borderWidth: 1, justifyContent: 'center', alignItems: 'center', borderRadius: 32, backgroundColor: black, zIndex: 3 }}>
                                                                <ImageBackground source={{ uri: `${reject.jobid.image}` }} style={{ width: 35, height: 35 }}></ImageBackground>
                                                            </View>
                                                            <View style={{ flex: 1, marginHorizontal: 20, justifyContent: 'center' }}>
                                                                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{reject.jobid.name}</Text>
                                                                <View style={{ flexDirection: 'row', marginTop: 5 }}><Text>{reject.jobid.location} - </Text><Text style={{ color: green }}>{reject.jobid.status}</Text></View>
                                                            </View>
                                                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                                <Text style={{ marginBottom: 5 }}><GiftIcon /></Text>
                                                                <Text>${reject.jobid.salary}</Text>
                                                            </View>
                                                        </View>
                                                    </TouchableHighlight>
                                                )
                                            })
                                                : <Text style={{ color: dash_font }}>You have no rejections</Text>
                                        }
                                    </View>
                                    :
                                    active === 3 ?
                                        <View>
                                            {
                                                accepted && accepted.length > 0 ? accepted.map((accept, index) => {
                                                    return (
                                                        <TouchableHighlight underlayColor={white} onPress={openModal} key={index} style={{ backgroundColor: white, padding: 15, borderRadius: 10, marginBottom: 20 }}>
                                                            <View style={{ flexDirection: 'row', justifyContent: "space-between" }}>
                                                                <View style={{ width: 64, height: 64, borderWidth: 1, justifyContent: 'center', alignItems: 'center', borderRadius: 32, backgroundColor: black, zIndex: 3 }}>
                                                                    <ImageBackground source={{ uri: `${accept.jobid.image}` }} style={{ width: 35, height: 35 }}></ImageBackground>
                                                                </View>
                                                                <View style={{ flex: 1, marginHorizontal: 20, justifyContent: 'center' }}>
                                                                    <Text style={{ fontSize: 20, fontWeight: 'bold' }}>{accept.jobid.name}</Text>
                                                                    <View style={{ flexDirection: 'row', marginTop: 5 }}><Text>{accept.jobid.location} - </Text><Text style={{ color: green }}>{accept.jobid.status}</Text></View>
                                                                </View>
                                                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                                    <Text style={{ marginBottom: 5 }}><GiftIcon /></Text>
                                                                    <Text>${accept.jobid.salary}</Text>
                                                                </View>
                                                            </View>
                                                        </TouchableHighlight>
                                                    )
                                                })
                                                    : <Text style={{ color: dash_font }}>You have no acceptance yet</Text>
                                            }
                                        </View>
                                        : null
                        }
                    </View>
                </View>
            </ScrollView>
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
}

Applications = connect(mapStateToProps, { openModal, getProfile })(Applications);

export { Applications };