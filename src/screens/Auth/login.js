import React, { useState } from 'react';
import { View, Text, TextInput, TouchableHighlight, TouchableOpacity, ScrollView } from 'react-native';
import { theme } from '../../theme';
import { Eye, PasswordOpen } from '../../assets/icons';
import validator from '../../utils/validator';
import { login } from '../../store/actions/authActions';
import { connect } from 'react-redux';

let Login = (props) => {
    const [data, setData] = useState({ email: null, password: null, emailError: null, passwordError: null });

    const [togglePass, setTogglePass] = useState(true);
    const { lightFont, splash, yellow, fontTwo, inputBackground, green, black, errorOutline } = theme();

    const handleChange = (name, val) => {
        setData({ ...data, [name]: val, emailError: null, passwordError: null })
    }
    
    const onSubmit = () => {
        if (!validator('email', data.email)) {
            setData({ ...data, emailError: 'Invalid email address' })
        } else if (!validator('password', data.password)) {
            setData({ ...data, passwordError: 'Minimum of six characters' })
        } else {
            props.login(data).then(result => {
                if (result.success) {
                    props.navigation.navigate('Main');
                }
            })
                .catch(err => console.log('ss', err));
        }
    }

    return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
            <ScrollView style={{ paddingHorizontal: 25 }}>
                <View style={{ marginTop: 80 }}>
                    <View style={{ marginBottom: 80, alignItems: "center" }}>
                        <View><Text style={{ fontSize: 30, color: lightFont }}>We<Text style={{ fontWeight: 'bold', fontSize: 30, color: fontTwo }}>Japa</Text></Text></View>
                    </View>
                    <View style={{ marginBottom: 10 }}>
                        <View style={{ borderWidth: 1, borderRadius: 10, paddingLeft: 20, paddingVertical: 6, backgroundColor: '#f2e9e9', borderColor: data.emailError ? errorOutline : '#f2e9e9' }}>
                            <TextInput
                                placeholder='Your Email'
                                onChangeText={(text) => handleChange('email', text)}
                                style={{ fontWeight: "bold" }}
                                value={data.email}
                            />
                        </View>
                        <Text style={{ paddingHorizontal: 30, paddingVertical: 6, color: black, fontWeight: 'bold' }}>{data.emailError ? data.emailError : null}</Text>
                    </View>
                    <View style={{ marginBottom: 15 }}>
                        <View style={{ borderWidth: 1, borderRadius: 10, paddingLeft: 20, paddingRight: 14, paddingVertical: 4, backgroundColor: '#f2e9e9', borderColor: data.passwordError ? errorOutline : '#f2e9e9', flexDirection: 'row', alignItems: 'center' }}>
                            <TextInput
                                placeholder='Password'
                                onChangeText={(text) => handleChange('password', text)}
                                value={data.password}
                                secureTextEntry={togglePass}
                                style={{ flex: 1 }}
                            />
                            <TouchableOpacity underlayColor={green} onPress={() => setTogglePass(!togglePass)}>
                                {togglePass ? <PasswordOpen /> : <Eye />}
                            </TouchableOpacity>
                        </View>
                        <Text style={{ paddingHorizontal: 30, paddingVertical: 6, color: black, fontWeight: 'bold' }}>{data.passwordError ? data.passwordError : null}</Text>
                    </View>
                    <View style={{ marginBottom: 15, paddingRight: 10, flexDirection: 'row', justifyContent: 'flex-end' }}>

                        <TouchableOpacity><Text style={{ color: lightFont }}>Forgot Password?</Text></TouchableOpacity>
                    </View>
                    <TouchableHighlight underlayColor={yellow} onPress={onSubmit} style={{ backgroundColor: yellow, marginBottom: 30, marginTop: 20, padding: 20, borderRadius: 10, flexDirection: 'row', justifyContent: 'center' }}>
                        <Text style={{ fontWeight: '300' }}>Sign in</Text>
                    </TouchableHighlight>
                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}><Text style={{ color: lightFont }}>Don't have an account? </Text><TouchableOpacity onPress={() => props.navigation.navigate('Signup')}><Text style={{ color: splash, fontWeight: "bold" }}>Sign up</Text></TouchableOpacity></View>
                </View>
            </ScrollView>
        </View>
    )
}

Login = connect(null, { login })(Login);

export { Login };