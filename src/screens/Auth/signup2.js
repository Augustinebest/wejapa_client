import React, { useState } from 'react';
import { View, Text, TouchableHighlight, TextInput, TouchableOpacity, CheckBox } from 'react-native';
import { BackwardIcon, PasswordOpen, Eye } from '../../assets/icons';
import { theme } from '../../theme';
import { connect } from 'react-redux';
import { setAuthState, signup } from '../../store/actions/authActions';
import { alertON } from '../../store/actions/privelegeActions';
import validator from '../../utils/validator';

let Signup2 = (props) => {
    const [togglePass, setTogglePass] = useState(true);
    const [isSelected, setSelection] = useState(false);
    const [err, setErr] = useState({ passwordErr: null });
    const { white, inputBackground, lightFont, green, inactive, yellow, errorOutline, black } = theme();

    const state = props.auth;

    const handleChange = (name, value) => {
        props.setAuthState({ [name]: value })
        setErr({ ...err, passwordErr: null });
    }

    const onSubmit = () => {
        // const message = 'Please fix the error in red before submitting.';
        if (state.password.length === 0 || state.password === null) {
            setErr({ ...err, passwordErr: 'Please enter a password' })
            props.alertON(message)
        } else if (!validator('password', state.password)) {
            setErr({ ...err, passwordErr: 'Minimum of six characters' })
            props.alertON(message)
        } else if (!isSelected) {
            props.alertON('Agree to terms before submiting');
        } else {
            props.signup(state).then(res => {
                props.navigation.navigate('Login')
                props.alertON('Your registration have been received successfully')
            }).catch(err => console.log(err))
        }
    }

    return (
        <View style={{ flex: 1, backgroundColor: white }}>
            <View style={{ paddingBottom: 30, justifyContent: 'flex-start', flexDirection: 'row' }}><TouchableHighlight underlayColor={null} onPress={() => props.navigation.goBack()} style={{ padding: 15 }}><BackwardIcon /></TouchableHighlight></View>
            <View style={{ paddingHorizontal: 20 }}>
                <View style={{ justifyContent: 'center', flexDirection: 'row', marginBottom: 20 }}><Text style={{ fontSize: 18, color: lightFont, fontWeight: 'bold', borderRadius: 50, borderWidth: 1, padding: 4, backgroundColor: inputBackground, borderColor: inputBackground }}>2/2</Text></View>
                <View style={{ marginBottom: 20 }}>
                    <View style={{ borderWidth: 1, marginTop: 30, borderRadius: 10, paddingLeft: 20, paddingRight: 14, paddingVertical: 4, backgroundColor: inputBackground, borderColor: err.passwordErr ? errorOutline : inputBackground, flexDirection: 'row', alignItems: 'center' }}>
                        <TextInput
                            placeholder='Password'
                            secureTextEntry={togglePass}
                            style={{ flex: 1 }}
                            onChangeText={(text) => handleChange('password', text)}
                            value={state.password}
                        />
                        <TouchableOpacity underlayColor={green} onPress={() => setTogglePass(!togglePass)}>
                            {togglePass ? <PasswordOpen /> : <Eye />}
                        </TouchableOpacity>
                    </View>
                    <Text style={{ paddingHorizontal: 30, paddingTop: 4, color: black, fontWeight: 'bold' }}>{err.passwordErr ? err.passwordErr : null}</Text>
                </View>
                <View style={{ flexDirection: 'row', padding: 0, marginTop: 120 }}>
                    <CheckBox
                        value={isSelected}
                        onValueChange={() => setSelection(!isSelected)}
                    />
                    <View style={{ flex: 1, marginLeft: 25 }}><Text style={{ color: inactive, fontSize: 12 }}>By checking below to signup, you're agreeing to <Text style={{ color: green, fontSize: 12, marginLeft: 10 }}>our terms of service</Text></Text></View></View>
                <TouchableHighlight underlayColor={yellow} onPress={onSubmit} style={{ backgroundColor: yellow, marginBottom: 30, marginTop: 50, padding: 20, borderRadius: 10, flexDirection: 'row', justifyContent: 'center' }}>
                    <Text style={{ fontWeight: '300' }}>Sign up</Text>
                </TouchableHighlight>
            </View>
        </View>
    )
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
}

Signup2 = connect(mapStateToProps, { setAuthState, alertON, signup })(Signup2);

export { Signup2 }