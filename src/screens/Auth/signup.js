import React, { useState } from 'react';
import { View, Text, ScrollView, TextInput, TouchableHighlight, TouchableOpacity } from 'react-native';
import { theme } from '../../theme';
import Dropdown from '../../components/dropdown';
import { connect } from 'react-redux';
import { setAuthState } from '../../store/actions/authActions';
import { alertON } from '../../store/actions/privelegeActions';
import validator from '../../utils/validator';

let Signup = (props) => {
    const [dropdown, showDropdown] = useState(false);
    const [err, setErr] = useState({ fullNameErr: null, emailErr: null, phoneErr: null, portfolioErr: null, stackoverflowErr: null, resumeErr: null, experienceErr: null })
    const [level, setLevel] = useState(null)
    const { inputBackground, green, white, lightFont, errorOutline, black } = theme();
    const chooseLevel = (option) => {
        setLevel(option)
        setErr({...err, experienceErr: null})
        showDropdown(!dropdown)
        props.setAuthState({ experience: option })
    }

    const handleChange = (name, val) => {
        props.setAuthState({ [name]: val });
        setErr({ fullNameErr: null, emailErr: null, phoneErr: null, portfolioErr: null, stackoverflowErr: null, resumeErr: null, experienceErr: null })
    }

    const goNext = () => {
        const message = 'Please fix the error in red before submitting.';
        const { fullName, email, phone, portfolio, stackoverflow, resume, experience } = props.auth;
        if (fullName === null) {
            setErr({ ...err, fullNameErr: 'Full Name is Required' })
            props.alertON(message)
        } else if (!validator('email', email)) {
            setErr({ ...err, emailErr: 'Invalid email address' })
            props.alertON(message)
        } else if (!validator('phone', phone)) {
            setErr({ ...err, phoneErr: 'Enter Number with country code' })
            props.alertON(message)
        } else if (!validator('url', portfolio)) {
            setErr({ ...err, portfolioErr: 'Github/Portfolio/Website url required' })
        } else if (!validator('url', resume)) {
            setErr({ ...err, resumeErr: 'Resume link is required' })
            props.alertON(message)
        } else if (stackoverflow && stackoverflow.length > 0 && !validator('url', stackoverflow)) {
            setErr({ ...err, stackoverflowErr: 'Invalid Stackoverflow link' })
            props.alertON(message)
        } else if (experience === null || experience === 'Experience level') {
            setErr({ ...err, experienceErr: 'Select Experience level' })
            props.alertON(message)
        } else {
            props.navigation.navigate('Signup2')
        }
    }

    return (
        <>
            {dropdown ? <Dropdown hide={() => showDropdown(!dropdown)} option={chooseLevel} /> : null}
            <View style={{ flex: 1 }}>
                <ScrollView style={{ paddingHorizontal: 25 }}>
                    <View style={{ justifyContent: 'center', flexDirection: 'row', marginBottom: 45, marginTop: 20 }}><Text style={{ fontSize: 30, color: lightFont }}>Signup</Text></View>
                    <View style={{ justifyContent: 'center', flexDirection: 'row', marginBottom: 20 }}><Text style={{ fontSize: 18, color: lightFont, fontWeight: 'bold', borderRadius: 50, borderWidth: 1, padding: 4, backgroundColor: inputBackground, borderColor: inputBackground }}>1/2</Text></View>
                    <View style={{ marginBottom: 20 }}>
                        <View style={{ borderWidth: 1, borderRadius: 10, paddingLeft: 20, paddingVertical: 4, backgroundColor: inputBackground, borderColor: err.fullNameErr ? errorOutline : inputBackground }}>
                            <TextInput
                                placeholder='Your Full Name'
                                onChangeText={(text) => handleChange('fullName', text)}
                                style={{ fontWeight: "bold" }}
                                value={props.auth.fullName}
                            />
                        </View>
                        <Text style={{ paddingHorizontal: 30, paddingTop: 4, color: black, fontWeight: 'bold' }}>{err.fullNameErr ? err.fullNameErr : null}</Text>
                    </View>
                    <View style={{ marginBottom: 20 }}>
                        <View style={{ borderWidth: 1, borderRadius: 10, paddingLeft: 20, paddingVertical: 4, backgroundColor: inputBackground, borderColor: err.emailErr ? errorOutline : inputBackground }}>
                            <TextInput
                                placeholder='Your Email'
                                onChangeText={(text) => handleChange('email', text)}
                                style={{ fontWeight: "bold" }}
                                value={props.auth.email}
                            />
                        </View>
                        <Text style={{ paddingHorizontal: 30, paddingTop: 4, color: black, fontWeight: 'bold' }}>{err.emailErr ? err.emailErr : null}</Text>
                    </View>
                    <View style={{ marginBottom: 20 }}>
                        <View style={{ borderWidth: 1, borderRadius: 10, paddingLeft: 20, paddingVertical: 4, backgroundColor: inputBackground, borderColor: err.phoneErr ? errorOutline : inputBackground }}>
                            <TextInput
                                placeholder='Your Phone Number'
                                onChangeText={(text) => handleChange('phone', text)}
                                style={{ fontWeight: "bold" }}
                                value={props.auth.phone}
                            />
                        </View>
                        <Text style={{ paddingHorizontal: 30, paddingTop: 4, color: black, fontWeight: 'bold' }}>{err.phoneErr ? err.phoneErr : null}</Text>
                    </View>
                    <View style={{ marginBottom: 20 }}>
                        <View style={{ borderWidth: 1, borderRadius: 10, paddingLeft: 20, paddingVertical: 4, backgroundColor: inputBackground, borderColor: err.portfolioErr ? errorOutline : inputBackground }}>
                            <TextInput
                                placeholder='Github/Portfolio/Website url'
                                onChangeText={(text) => handleChange('portfolio', text)}
                                style={{ fontWeight: "bold" }}
                                value={props.auth.portfolio}
                            />
                        </View>
                        <Text style={{ paddingHorizontal: 30, paddingTop: 4, color: black, fontWeight: 'bold' }}>{err.portfolioErr ? err.portfolioErr : null}</Text>
                    </View>
                    <View style={{ marginBottom: 20 }}>
                        <View style={{ borderWidth: 1, borderRadius: 10, paddingLeft: 20, paddingVertical: 4, backgroundColor: inputBackground, borderColor: err.stackoverflowErr ? errorOutline : inputBackground }}>
                            <TextInput
                                placeholder='Stackoverflow (optional)'
                                onChangeText={(text) => handleChange('stackoverflow', text)}
                                style={{ fontWeight: "bold" }}
                                value={props.auth.stackoverflow}
                            />
                        </View>
                        <Text style={{ paddingHorizontal: 30, paddingTop: 4, color: black, fontWeight: 'bold' }}>{err.stackoverflowErr ? err.stackoverflowErr : null}</Text>
                    </View>
                    <View style={{ marginBottom: 20 }}>
                        <View style={{ borderWidth: 1, borderRadius: 10, paddingLeft: 20, paddingVertical: 4, backgroundColor: inputBackground, borderColor: err.resumeErr ? errorOutline : inputBackground }}>
                            <TextInput
                                placeholder='Resume Link: Drive, Dropbox, ...'
                                onChangeText={(text) => handleChange('resume', text)}
                                style={{ fontWeight: "bold" }}
                                value={props.auth.resume}
                            />
                        </View>
                        <Text style={{ paddingHorizontal: 30, paddingTop: 4, color: black, fontWeight: 'bold' }}>{err.resumeErr ? err.resumeErr : null}</Text>
                    </View>
                    <View style={{ marginBottom: 20 }}>
                        <TouchableOpacity underlayColor={inputBackground} onPress={() => showDropdown(!dropdown)} style={{ borderWidth: 1, borderRadius: 10, paddingLeft: 26, paddingVertical: 20, backgroundColor: inputBackground, borderColor: err.experienceErr ? errorOutline : inputBackground }}>
                            <Text style={{ opacity: level !== 'Experience level' ? 0.4 : null, fontSize: 15, fontWeight: level !== 'Experience level' ? 'bold' : null }}>{level === null ? 'Experience level' : level}</Text>
                        </TouchableOpacity>
                        <Text style={{ paddingHorizontal: 30, paddingTop: 4, color: black, fontWeight: 'bold' }}>{err.experienceErr ? err.experienceErr : null}</Text>
                    </View>
                    <TouchableHighlight underlayColor={green} onPress={goNext} style={{ backgroundColor: green, marginBottom: 30, marginTop: 20, padding: 20, borderRadius: 10, flexDirection: 'row', justifyContent: 'center' }}>
                        <Text style={{ fontWeight: '300', color: white }}>Next</Text>
                    </TouchableHighlight>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', marginBottom: 40 }}><Text style={{ color: lightFont }}>Already have an account? </Text><TouchableOpacity onPress={() => props.navigation.navigate('Login')}><Text style={{ color: green, fontWeight: "bold" }}>Sign in</Text></TouchableOpacity></View>
                </ScrollView>
            </View>
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth
    }
}

Signup = connect(mapStateToProps, { setAuthState, alertON })(Signup)

export { Signup };