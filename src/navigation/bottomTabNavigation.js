import React from 'react';
import { Text } from 'react-native'
import { BottomNav } from './common';
import { Jobs } from '../screens/dashboard'
import { Applications } from '../screens/dashboard';
import { SavedJobs } from '../screens/dashboard';
import { Profile } from '../screens/dashboard';
import { HomeIconA, HomeIconI, UserIconA, UserIconI, PinIconA, PinIconI, ApplicationIconA, ApplicationIconI } from '../assets/icons';
import { theme } from '../theme'

let NewStack = BottomNav;

const BottomNavigation = ({ navigation }) => {
    const { green, inactive } = theme();
    return (
        <NewStack.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused }) => {
                    if (route.name === 'Jobs') {
                        return focused ? <HomeIconA /> : <HomeIconI />
                    } else if (route.name === 'Applications') {
                        return focused ? <ApplicationIconA /> : <ApplicationIconI />
                    } else if (route.name === 'Saved Jobs') {
                        return focused ? <PinIconA /> : <PinIconI width="17"  height="17"/>
                    } else if (route.name === 'Profile') {
                        return focused ? <UserIconA /> : <UserIconI />
                    }
                }
            })}
            tabBarOptions={{
                activeTintColor: `${green}`,
                inactiveTintColor: `${inactive}`,
                allowFontScaling: true,
                adaptive: true,
                keyboardHidesTabBar: true,
                tabStyle: { paddingVertical: 5, paddingHorizontal: 10 },
                labelStyle: { fontFamily: 'Raleway', paddingHorizontal: 10 },
            }}
        >
            <NewStack.Screen name='Jobs' component={Jobs} />
            <NewStack.Screen name='Applications' component={Applications} />
            <NewStack.Screen name='Saved Jobs' component={SavedJobs} />
            <NewStack.Screen name='Profile' component={Profile} />
        </NewStack.Navigator>
    )
}

export default BottomNavigation;