import React, { useEffect } from 'react';
import { Stack } from './common';
import { Login } from '../screens/Auth'
import { Signup } from '../screens/Auth';
import { Signup2 } from '../screens/Auth';
import { Animated, Dimensions } from 'react-native';
import { Modal } from '../components/modal';
import { Alert } from '../components/alert';
import { Loader } from '../components/loader';

import BottomNavigation from './bottomTabNavigation';
import { Company, Review } from '../screens/others';
import { Search } from '../screens/others/search';
import { LatestRoles } from '../screens/others/latestRole';
import { SavedJobs } from '../screens/dashboard';

// actions
import { openModal, alertOFF } from '../store/actions/privelegeActions';
import { connect } from 'react-redux';

let deviceHeight = Dimensions.get('window').height

let Root = Stack;

const MainNavigation = (props) => {
    const [modalY, setModalY] = React.useState(new Animated.Value(deviceHeight))

    if(props.modal) {
        Animated.timing(modalY, {
            duration: 600,
            toValue: 0,
            useNativeDriver: true,
        }).start();
    } else {
        Animated.timing(modalY, {
            duration: 600,
            toValue: deviceHeight,
            useNativeDriver: true,
        }).start();
    }

    if(props.alert.status === true) {
        setTimeout(() => {
            props.alertOFF();
        }, 4000)
    }
    return (
        <>
            <Root.Navigator
                screenOptions={{
                    headerShown: false
                }}>
                <Root.Screen name='Login' component={Login} />
                <Root.Screen name='Signup' component={Signup} />
                <Root.Screen name='Main' component={BottomNavigation} />
                <Root.Screen name='Company' component={Company} />
                <Root.Screen name='Search' component={Search} />
                <Root.Screen name='LatestRoles' component={LatestRoles} />
                <Root.Screen name='Saved Jobs' component={SavedJobs} />
                <Root.Screen name='Signup2' component={Signup2} />
                <Root.Screen name='Review' component={Review} />
            </Root.Navigator>
            <Modal modalY={modalY} />
            {props.alert.status ? <Alert message={props.alert.message} /> : null}
            {props.loader ? <Loader /> : null}
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        modal: state.modal,
        loader: state.loader,
        alert: state.alert
    }
}

export default connect(mapStateToProps, { openModal, alertOFF })(MainNavigation);