import { OPEN_MODAL, CLOSE_MODAL, ALERT_ON, ALERT_OFF, GET_A_JOB  } from '../constant';
import Processor from '../../api/requestProcessor';

const openModal = (jobID) => (dispatch) => {
    dispatch({ type: OPEN_MODAL, payload: true });
    // console.log('OK: ',jobID._id)
    Processor.sendGet(`/user/getAJob/${jobID._id}`).then(res => {
        // console.log('A JOB: ', res.data.message)
        dispatch({ type: GET_A_JOB, payload: res.data.message })
    })
    .catch(err => {
        reject(err);
    })
}

const closeModal = () => (dispatch) => {
    dispatch({ type: CLOSE_MODAL, payload: false })
}

const alertOFF = () => (dispatch) => {
    dispatch({ type: ALERT_OFF, payload: {status: false, message: null} })
}

const alertON = (message) => (dispatch) => {
    dispatch({ type: ALERT_ON, payload: {status: true, message} })
}

export { openModal, closeModal, alertOFF, alertON }