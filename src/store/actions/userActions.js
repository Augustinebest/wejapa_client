import Processor from '../../api/requestProcessor';
import { GET_JOBS, SET_VALUES, ALERT_ON, LOADER_ON, LOADER_OFF, GET_A_JOB } from '../constant';

const dashboardGetAllJobs = () => (dispatch) => {
    return new Promise((resolve, reject) => {
        return Processor.sendGet('/user/getAllJobs').then(res => {
            if(res.data.success) {
                dispatch({ type: GET_JOBS, payload: res.data.message })
            }
        })
        .catch(err => {
            console.log(err);
        })
    })
}

const getProfile = () => (dispatch) => {
    return new Promise((resolve, reject) => {
        return Processor.sendGet('/user/profile').then(res => {
            if(res.data.status) {
                console.log('app: ', res.data.message)
                dispatch({ type: SET_VALUES, payload: res.data.message })
                resolve(true)
            }
        })
        .catch(err => {
            reject(err);
        })
    })
}

const updateProfile = (data) => (dispatch) => {
    const updates = {
        phone: data.phone,
        stackoverflow: data.stackoverflow,
        portfolio: data.portfolio,
        resume: data.resume
    }
    return new Promise((resolve, reject) => {
        dispatch({ type: LOADER_ON, payload: true  })
        return Processor.sendPost('/user/updateProfile', updates).then(res => {
            dispatch({ type: LOADER_OFF, payload: false  })
            dispatch({ type: ALERT_ON, payload: { status: true, message: res.data.message } })
            resolve(true)
        })
        .catch(err => {
            reject(err);
        })
    })
}

const applyForJob = (data) => (dispatch) => {
    console.log('apply2', data._id)
    const id = data._id
    dispatch({ type: LOADER_ON, payload: true  })
    return new Promise((resolve, reject) => {
        return Processor.sendPost(`/user/applyJob/${id}`, null).then(res => {
            console.log('JOB APPLY: ',res.data.message)
            dispatch({ type: LOADER_OFF, payload: false  })
            dispatch({ type: ALERT_ON, payload: { status: true, message: res.data.message.msg } })
            dispatch({ type: GET_A_JOB, payload: res.data.message })
            resolve(true);
        })
        .catch(err => {
            reject(err);
        })
    })
}

const search = (search) => (dispatch) => {
    if(search === '') search = ''
    return new Promise((resolve, reject) => {
        Processor.sendGet(`/user/search/${search}`).then(res => {
            if(res.data.success) {
                resolve({status: true, data: res.data.message});
            } else {
                resolve({status: false, data: []});
            }
        })
        .catch(err => {
            // reject(err.response);
            reject(err.response.data.message)
        })
    })
}

export { dashboardGetAllJobs, getProfile, updateProfile, applyForJob, search }