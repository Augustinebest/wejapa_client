import Processor from '../../api/requestProcessor';
import { LOADER_ON, LOADER_OFF, ALERT_ON, SET_VALUES } from '../constant';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios'

const login = (data) => (dispatch) => {
    return new Promise((resolve, reject) => {
        dispatch({ type: LOADER_ON, payload: true })
        return Processor.sendPost('/user/login', data).then(res => {
            dispatch({ type: LOADER_OFF, payload: false })
            if (res.data.success) {
                AsyncStorage.setItem('_wejapa_auth_token', res.data.token)
                axios.defaults.headers["x-access-token"] = res.data.token
                resolve({ success: true, result: res.data.message })
            } else {
                resolve({ success: false, result: res.data.message })
            }
        }).catch(err => {
            if (!err.response.data.success) {
                dispatch({ type: LOADER_OFF, payload: false })
                dispatch({ type: ALERT_ON, payload: { status: true, message: err.response.data.message } })
                reject({ success: false, result: err.response.data.message });
            }
        })
    })
}

const setAuthState = (data) => (dispatch) => {
    dispatch({ type: SET_VALUES, payload: data })
}

const signup = (data) => (dispatch) => {
    return new Promise((resolve, reject) => {
        return Processor.sendPost('/user/register', data).then(res => {
            console.log(res)
            if (res.data.success) {
                resolve({ success: true, result: res.data.message });
            } else {
                resolve({ success: false, result: res.data.message })
            }
        })
            .catch(err => {
                if (err.response.data.success) {
                    reject({ success: false, result: err.response.data.message });
                }
            })
    })
}

export { login, signup, setAuthState }