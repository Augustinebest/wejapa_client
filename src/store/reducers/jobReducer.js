import { GET_JOBS, GET_A_JOB } from '../constant';

const InitialState = {
    jobs: [],
    job: {}
};

export default JobReducer = (state = InitialState, action) => {
    switch (action.type) {
        case GET_JOBS:
            return { ...state, jobs: action.payload }
        case GET_A_JOB: 
            return { ...state, job: action.payload }
        default:
            return state
    }
}