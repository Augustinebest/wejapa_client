import { combineReducers } from 'redux';
import ModalReducer from './modalReducer';
import LoaderReducer from './loaderReducer';
import AlertReducer from './alertReducer';
import AuthReducer from './authReducer';
import JobReducer from './jobReducer'

export default RootReducer = combineReducers({
    modal: ModalReducer,
    loader: LoaderReducer,
    alert: AlertReducer,
    auth: AuthReducer,
    job: JobReducer
})