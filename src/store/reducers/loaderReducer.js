import { LOADER_ON, LOADER_OFF } from '../constant';

const InitialState = false;

export default LoaderReducer = (state = InitialState, action) => {
    switch (action.type) {
        case LOADER_ON:
            return action.payload
        case LOADER_OFF:
            return action.payload
        default:
            return state
    }
}