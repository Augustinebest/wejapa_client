import { ALERT_ON, ALERT_OFF } from '../constant';

const InitialState = {
    status: false,
    message: null
};

export default AlertReducer = (state = InitialState, action) => {
    switch (action.type) {
        case ALERT_ON:
            return {...state, ...action.payload}
        case ALERT_OFF:
            return {...state, ...action.payload}
        default:
            return state
    }
}