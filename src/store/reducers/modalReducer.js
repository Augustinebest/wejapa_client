import { OPEN_MODAL, CLOSE_MODAL } from '../constant';

const InitialState = false;

export default ModalReducer = (state = InitialState, action) => {
    switch(action.type) {
        case OPEN_MODAL: 
            return action.payload
        case CLOSE_MODAL: 
            return action.payload
        default: 
            return state
    }
}