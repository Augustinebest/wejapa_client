import { SET_VALUES } from "../constant"

const InitialState = {
    fullName: null,
    email: null,
    phone: null,
    portfolio: null,
    stackoverflow: null,
    resume: null,
    experience: null,
    password: null
}

export default AuthReducer = (state = InitialState, action) => {
    switch (action.type) {
        case SET_VALUES:
            return {...state, ...action.payload}
        default:
            return state
    }
}