import React, { useState } from 'react';
import { ThemeContext } from './themeContext';
import AsyncStorage from '@react-native-community/async-storage';
import { SafeAreaView } from 'react-native';

export const ThemeContextProvider = props => {

    const theme = {
        light: {
            type: 'light',
            primary: '#1C2D55',
            splash: '#5bad60',
            yellow: '#fed430',
            lightFont: 'rgba(0,0,0,0.3)',
            fontTwo: 'rgba(0,0,0,0.5)',
            inputBackground: '#f2e9e9',
            green: 'rgb(99, 192, 105)',
            white: '#fff',
            background: '#eee',
            black: '#000',
            inactive: 'rgba(0,0,0,0.4)',
            alertbg: 'rgba(0,0,0,0.7)',
            errorOutline: '#f00',
            inputBackgroundO: '#f2e9e9',

            dash_bg: '#fff',
            dash_font: '#000',
            dash_border: '#f2e9e9',
            dash_inputBackground: '#f2e9e9',
            dash_signOut_btn: '#000',
            dash_border2: '#f2e9e9',
            applications_bg: 'rgb(99, 192, 105)',
            dash_inputBackground_2: '#f2e9e9',
            dash_font_2: '#000'
        },
        dark: {
            type: 'dark',
            primary: '#1C2D55',
            splash: '#5bad60',
            yellow: '#fed430',
            lightFont: 'rgba(0,0,0,0.3)',
            fontTwo: 'rgba(0,0,0,0.5)',
            green: 'rgb(99, 192, 105)',
            white: '#fff',
            background: '#eee',
            black: '#000',
            inactive: 'rgba(0,0,0,0.4)',
            alertbg: 'rgba(0,0,0,0.7)',
            errorOutline: '#f00',

            dash_bg: '#000',
            dash_font: '#fff',
            dash_border: 'rgb(99, 192, 105)',
            dash_inputBackground: '#000',
            dash_signOut_btn: '#f00',
            dash_border2: '#fff',
            applications_bg: '#000',
            dash_inputBackground_2: '#fff',
            dash_font_2: '#fff'
        }
    }
    
    const setTheme = type => {
        setState({ ...state, theme: type === 'dark' ? theme.light : theme.dark });
    }

    const initState = () => {
        let setLin = AsyncStorage.getItem('theme');
        console.log('localstorage data on load is: ', setLin)
        if (setLin && setLin.theme) {
            return {
                theme: theme[setLin.type],
                setTheme: setTheme
            }
        }
        else {
            return {
                theme: theme.light,
                setTheme: setTheme
            }
        }
    }

    const [state, setState] = useState(initState)

    return (
        <ThemeContext.Provider value={state}>
            <SafeAreaView style={{ flex: 1, backgroundColor: state.theme.background }}>
                {props.children}
            </SafeAreaView>
        </ThemeContext.Provider>
    )
}