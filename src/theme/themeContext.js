import React from 'react';

export const ThemeContext = React.createContext({
    theme: {
        type: 'light',
        splash: '#5bad60',
        yellow: '#fed430',
        lightFont: 'rgba(0,0,0,0.3)',
        fontTwo: 'rgba(0,0,0,0.5)',
        inputBackground: '#f2e9e9',
        green: 'rgb(99, 192, 105)',
        white: '#fff',
        black: '#000',
        background: '#eee',
        inactive: 'rgba(0,0,0,0.4)',
        alertbg: 'rgba(0,0,0,0.7)',
        errorOutline: '#f00',
        inputBackgroundO: '#f2e9e9',

        dash_bg: '#fff',
        dash_font: '#000',
        dash_border: 'rgb(99, 192, 105)',
        dash_inputBackground: '#f2e9e9',
        dash_signOut_btn: '#000',
        dash_border2: '#f2e9e9',
        applications_bg: 'rgb(99, 192, 105)',
        dash_inputBackground_2: '#f2e9e9',
        dash_font_2: '#000'
    },
    setTheme: () => { }
})