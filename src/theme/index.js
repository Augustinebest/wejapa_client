import React from 'react';
import { ThemeContext } from './themeContext';

export function theme() {
    return React.useContext(ThemeContext).theme;
}

export function setTheme(){
    return React.useContext(ThemeContext).setTheme;
}

export * from './themeContext';
export * from './themeContextProvider';