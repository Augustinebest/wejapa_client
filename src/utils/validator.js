export default(type, value) => {
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const urlRegex = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/;
    const fullNameRegex = /^[a-zA-Z ]+$/;
    const phoneRegex = /\+\d{1,3}\d{7,}/;
    const passwordRegex = /^.{6,}$/;

    if(type === 'email') {
        if(emailRegex.test(value)) return true;
        return false
    }
    if(type === 'password') {
        if(passwordRegex.test(value)) return true;
        return false
    }
    if(type === 'url') {
        if(urlRegex.test(value)) return true;
        return false;
    }
    if(type === 'fullName') {
        if(fullNameRegex.test(value)) return true;
        return false;
    }
    if(type === 'phone') {
        if(phoneRegex.test(value)) return true;
        return false;
    }
}