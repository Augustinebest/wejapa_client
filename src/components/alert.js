import React from 'react';
import { View, Text } from 'react-native';
import { theme } from '../theme';

const Alert = (props) => {
    const { white, alertbg } = theme();
    return (
    <View style={{backgroundColor: alertbg, paddingHorizontal: 10, paddingVertical: 10, zIndex: 20, position: "absolute", bottom: 0, right: 0, left: 0}}><Text style={{color: white, fontSize: 14}}>{props.message}</Text></View>
    )
}

export {Alert};