import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

const Dropdown = (props) => {
    const options = ['Experience level', 'Junior: 1 - 2 years', 'Mid level: 3 - 5 years', 'Senior: > 5 years'];
    const sendOption = (option) => {
        props.option(option)
    }
    return (
        <TouchableOpacity onPress={props.hide} style={{ backgroundColor: 'rgba(0,0,0,0.1)', flex: 1, position: 'absolute', top: 0, left: 0, bottom: 0, right: 0, zIndex: 1 }}>
            <View style={{ borderColor: '#eee', borderWidth: 1, backgroundColor: '#fff', shadowColor: '#eee', shadowRadius: 10, shadowOpacity: 1, paddingVertical: 10, position: 'absolute', bottom: 30, left: 50, right: 50 }}>
                {
                    options && options.map((option, index) => {
                        return (
                            <TouchableOpacity onPress={() => sendOption(option)} key={index} style={{ paddingVertical: 15, paddingHorizontal: 15 }}>
                                <Text>{option}</Text>
                            </TouchableOpacity>
                        )
                    })
                }
            </View>
        </TouchableOpacity>
    )
}

export default Dropdown;