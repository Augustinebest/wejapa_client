import React, { useState } from 'react';
import { Animated, View, Text, TouchableHighlight, StyleSheet, Dimensions, ScrollView, ImageBackground } from 'react-native';
import { theme } from '../theme';
import { closeModal } from '../store/actions/privelegeActions';
import { applyForJob } from '../store/actions/userActions';
import { connect } from 'react-redux';
let deviceHeight = Dimensions.get('window').height
var deviceWidth = Dimensions.get('window').width;

let Modal = (props) => {
    const [active, setActive] = useState(1)
    const { black, white, green, inactive, dash_inputBackground, dash_font } = theme()
    const closeModal = () => {
        props.closeModal();
    }
    const job = props.job.job.job;
    const status = props.job.job.userApplied;

    const applyForJob = () => {
        props.applyForJob(job)
    }
    return (
        <Animated.View style={[styles.modal, { transform: [{ translateY: props.modalY }] }]}>
            <TouchableHighlight onPress={closeModal} underlayColor={black} style={{ backgroundColor: black, ...styles.button }}>
                <Text style={{ backgroundColor: white, width: 45, borderRadius: 10, height: 4 }}></Text>
            </TouchableHighlight>
            <ScrollView style={{ flex: 1, backgroundColor: white, borderTopRightRadius: 10, borderTopLeftRadius: 10, zIndex: 30, marginTop: -10, paddingHorizontal: 20 }}>
                <View style={{ marginBottom: 100 }}>
                    <View style={{ marginTop: 50, alignItems: 'center' }}>
                        <View style={{ width: 64, height: 64, borderWidth: 1, justifyContent: 'center', alignItems: 'center', borderRadius: 32, backgroundColor: black, zIndex: 3 }}>
                            <ImageBackground source={job && { uri: `${job.image}` }} style={{ width: 35, height: 35 }}></ImageBackground>
                        </View>
                        <Text style={{ marginTop: 30, fontWeight: 'bold', color: black, fontSize: 24, opacity: 0.7 }}>{job && job.name}</Text>
                        <Text style={{ color: inactive, marginTop: 10 }}>{job && job.location}</Text>
                        <Text style={{ backgroundColor: green, marginTop: 15, paddingHorizontal: 25, paddingVertical: 4, color: white, borderRadius: 20 }}>{job && job.status}</Text>
                    </View>
                    <View style={{ marginTop: 55, flexDirection: "row", justifyContent: "space-between" }}>
                        <View style={{ backgroundColor: dash_inputBackground, padding: 20, width: '30%', alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{color: dash_font}}>A</Text>
                            <Text style={{ marginTop: 5, color: dash_font }}>{job && job.workTime}</Text>
                        </View>
                        <View style={{ backgroundColor: dash_inputBackground, padding: 20, width: '30%', alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{color: dash_font}}>A</Text>
                            <Text style={{ marginTop: 5, color: dash_font }}>${job && job.salary}</Text>
                        </View>
                        <View style={{ backgroundColor: dash_inputBackground, padding: 20, width: '30%', alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{color: dash_font}}>A</Text>
                            <Text style={{ marginTop: 5, textAlign: "center", color: dash_font }}>{job && job.workType}</Text>
                        </View>
                    </View>
                    <View style={{ marginTop: 30, flexDirection: 'row', justifyContent: "space-between" }}>
                        <TouchableHighlight underlayColor={null} onPress={() => { setActive(1) }}><Text style={{ paddingBottom: 6, borderBottomColor: green, borderBottomWidth: active === 1 ? 4 : 0, fontSize: 18, color: active === 1 ? black : inactive }}>Description</Text></TouchableHighlight>
                        <TouchableHighlight underlayColor={null} onPress={() => { setActive(2) }}><Text style={{ paddingBottom: 6, borderBottomColor: green, borderBottomWidth: active === 2 ? 4 : 0, fontSize: 18, color: active === 2 ? black : inactive }}>Benefits</Text></TouchableHighlight>
                        <TouchableHighlight underlayColor={null} onPress={() => { setActive(3) }}><Text style={{ paddingBottom: 6, borderBottomColor: green, borderBottomWidth: active === 3 ? 4 : 0, fontSize: 18, color: active === 3 ? black : inactive }}>Experience</Text></TouchableHighlight>
                    </View>
                    <View style={{ marginTop: 30 }}>
                        {
                            active === 1 ?
                                <View><Text style={{ lineHeight: 20, color: inactive, fontSize: 18 }}>{job && job.description}</Text></View>
                                :
                                active === 2 ?
                                    <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                                        <View style={{ backgroundColor: white, padding: 20, alignItems: 'center', width: '28%' }}>
                                            <Text>A</Text>
                                            <Text style={{ textAlign: 'center' }}>{job && job.benefits}</Text>
                                        </View>
                                    </View>
                                    :
                                    active === 3 ?
                                        <View><Text style={{ lineHeight: 20, color: inactive, fontSize: 18 }}>{job && job.experience}</Text></View>
                                        : null
                        }
                    </View>
                    <TouchableHighlight onPress={!status ? applyForJob : null} underlayColor={inactive} style={{ backgroundColor: !status ? green : inactive, marginTop: 30, padding: 20, borderRadius: 10, flexDirection: 'row', justifyContent: 'center' }}>
                        <Text style={{ fontWeight: '300', color: white }}>{!status ? 'Apply now' : 'Applied'}</Text>
                    </TouchableHighlight>
                    <TouchableHighlight underlayColor={white} onPress={() => alert("This feature ain't working yet")} style={{ backgroundColor: '#d1d1d1', marginTop: 15, padding: 20, borderRadius: 10, flexDirection: 'row', justifyContent: 'center' }}>
                        <Text style={{ fontWeight: '300', color: green, zIndex: 4, opacity: 1 }}>Save JOb</Text>
                    </TouchableHighlight>
                </View>
            </ScrollView>
        </Animated.View>
    )
}

const mapStateToProps = (state) => {
    return {
        job: state.job
    }
}

Modal = connect(mapStateToProps, { closeModal, applyForJob })(Modal);

export { Modal }

const styles = StyleSheet.create({
    modal: {
        height: deviceHeight,
        width: deviceWidth,
        position: 'absolute',
        top: 0,
        left: 0,
        flex: 1,
        zIndex: 3,
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10
    },
    button: {
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 20,
        justifyContent: 'center',
    },
})