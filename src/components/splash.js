import React from 'react';
import { View, Text, ImageBackground } from 'react-native';
import { theme } from '../theme';

const Splash = () => {
    const { splash, error } = theme()
    return (
        <View style={{ backgroundColor: '#5bad60', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <View style={{ borderWidth: 1, backgroundColor: 'black', padding: 30, alignItems: 'center' }}>
                <ImageBackground source={require('../assets/myweja.png')} style={{ width: 50, height: 50 }} />
                <Text style={{ color: 'white', marginTop: 20, fontSize: 20 }}>WeJapa</Text>
            </View>
        </View>
    )
}

export default Splash;