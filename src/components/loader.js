import React from 'react';
import { View, Text, Dimensions } from 'react-native';
import { Loading } from '../assets/icons';
import { theme } from '../theme';

let deviceHeight = Dimensions.get('window').height
let deviceWidth = Dimensions.get('window').width;

let Loader = () => {
    const { inactive, white } = theme();
    return (
        <View style={{ position: 'absolute', top: 0, left: 0, backgroundColor: inactive, flex: 1, width: deviceWidth, height: deviceHeight, justifyContent: "center", alignItems: "center", zIndex: 10 }}>
            <View style={{backgroundColor: white, paddingHorizontal: 30, paddingVertical: 35, width: '80%', borderRadius: 10, justifyContent: "center", alignItems: "center"}}>
                <Loading />
            </View>
        </View>
    )
}

export { Loader }