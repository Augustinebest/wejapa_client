/**
 * @format
 */
import React from 'react';
import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import storeConfig from './src/store/store.config';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import Splash from './src/components/splash';

AsyncStorage.getItem("_wejapa_auth_token").then(result => {
    if(result) {
        axios.defaults.headers["x-access-token"] = result;
        // console.log('checking token in asyncstorage: ', result)
    }
})

const { store, persistor } = storeConfig();

const ProviderComponent = () => {
    return (
        <Provider store={store}>
            <PersistGate persistor={persistor} loading={<Splash />}>
                <App />
            </PersistGate>
        </Provider>
    )
}

AppRegistry.registerComponent(appName, () => ProviderComponent);
